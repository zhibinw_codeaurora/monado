u/distortion: Improve Index distortion and tidy code. While this touches the
Vive distortion code all Vive headsets seems to have the center set to the same
for each channel so doesn't help them. And Vive doesn't have the extra
coefficient that the Index does so no help there either.
