main: Lower priority on sRGB format. This works around a bug in the OpenXR CTS
and mirrors better what at least on other OpenXR runtime does.
