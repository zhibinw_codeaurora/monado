OpenXR: Be more relaxed with Quat validation, spec says within 1% of unit
length, normalize if not within float epsilon.
